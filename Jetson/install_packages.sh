#back to home directory
cd ~

#install pip
pip3 install --upgrade pip

#install packages
pip3 install pandas
pip3 install seaborn
pip3 install scikit-learn
pip3 install matplotlib
pip3 install medmnist
pip3 install opencv-python
pip3 install Pillow
pip3 install ipykernel
pip3 install tensorflow-datasets
pip3 install ipywidgets